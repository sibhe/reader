package com.imajinator.reader;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public interface BaseView<P> {

    void setPresenter(P presenter);
}
