package com.imajinator.reader.main;

import android.support.v7.util.DiffUtil;

import com.imajinator.reader.data.Article;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class DiffCallback extends DiffUtil.Callback{

    List<Article> mOldArticles;
    List<Article> mNewArticles;

    public DiffCallback(List<Article> old, List<Article> newer) {
        mOldArticles = old;
        mNewArticles = newer;
    }

    @Override
    public int getOldListSize() {
        return mOldArticles.size();
    }

    @Override
    public int getNewListSize() {
        return mNewArticles.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldArticles.get(oldItemPosition) == mNewArticles.get(newItemPosition);
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldArticles.get(oldItemPosition).equals(mNewArticles.get(newItemPosition));
    }
}
