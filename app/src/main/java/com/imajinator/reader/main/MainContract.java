package com.imajinator.reader.main;

import com.imajinator.reader.BasePresenter;
import com.imajinator.reader.BaseView;
import com.imajinator.reader.data.Article;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class MainContract {

    interface Presenter extends BasePresenter<View> {
        void getArticles();

        void isUserPickAnySource();
    }

    interface View extends BaseView<Presenter> {
        void showArticles(List<Article> articles);

        void showErrorView();

        void goToSourcePicker();

        void hideProgress();

        void showProgress();
    }
}
