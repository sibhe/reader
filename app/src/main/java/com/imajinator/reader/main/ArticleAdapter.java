package com.imajinator.reader.main;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.v7.util.DiffUtil;
import android.util.Log;
import android.view.View;

import com.imajinator.reader.BaseBindingAdapter;
import com.imajinator.reader.GlideApp;
import com.imajinator.reader.R;
import com.imajinator.reader.data.Article;
import com.imajinator.reader.databinding.ItemNewsBinding;
import com.imajinator.reader.util.DateUtil;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

class ArticleAdapter extends BaseBindingAdapter {
    private List<Article> mArticles;
    private Context mContext;
    private View.OnClickListener mListener;

    ArticleAdapter(Context context, List<Article> articles, View.OnClickListener listener) {
        mArticles = articles;
        mContext = context;
        mListener = listener;
    }

    void updateList(List<Article> newList) {
        DiffUtil.DiffResult diffResult = DiffUtil
                .calculateDiff(new DiffCallback(mArticles, newList));
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    protected void updateBinding(ViewDataBinding binding, int position) {
        ItemNewsBinding itemBinding = (ItemNewsBinding) binding;
        Article article = mArticles.get(position);
        itemBinding.tvTitle.setText(article.getTitle());
        itemBinding.tvDate.setText(DateUtil.adjustTimePattern(article.getPublishedAt(),
                "yyyy-MM-dd'T'HH:mm:ss'Z'", "MMM dd"));
        itemBinding.tvSource.setText(article.getAuthor());
        Log.d("title -->", article.getTitle());
        GlideApp.with(mContext)
                .load(article.getUrlToImage())
                .placeholder(R.color.gray)
                .into(itemBinding.ivNews);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.item_news;
    }

    @Override
    protected View.OnClickListener getOnClickListener() {
        return mListener;
    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }
}
