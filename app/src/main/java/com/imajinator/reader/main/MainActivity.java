package com.imajinator.reader.main;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.imajinator.reader.BaseBindingViewHolder;
import com.imajinator.reader.Injection;
import com.imajinator.reader.R;
import com.imajinator.reader.customtabs.CustomtabHelper;
import com.imajinator.reader.data.Article;
import com.imajinator.reader.databinding.ActivityMainBinding;
import com.imajinator.reader.sourcepicker.SourcePickerActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private ActivityMainBinding binding;
    private MainContract.Presenter presenter;
    private ArticleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.rvNews.setLayoutManager(new LinearLayoutManager(this));

        presenter = new MainPresenter(
                Injection.provideRepository(getApplicationContext()),
                Injection.provideScheduler()
        );

        presenter.bind(this);
        presenter.isUserPickAnySource();
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showArticles(List<Article> articles) {
        if (adapter == null) {
            adapter = new ArticleAdapter(this, articles, v -> {
                BaseBindingViewHolder holder = (BaseBindingViewHolder) v.getTag();
                Article article = articles.get(holder.getAdapterPosition());
                SourcePickerActivity.intent(this);
                CustomtabHelper.openUrl(this, article.getUrl());
            });
            binding.rvNews.setAdapter(adapter);
        } else {
            adapter.updateList(articles);
        }
    }

    @Override
    public void goToSourcePicker() {
        finish();
        SourcePickerActivity.intent(this);
    }

    @Override
    public void showErrorView() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unbind();
    }


    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    public static void intent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_pick_channel) {
            goToSourcePicker();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
