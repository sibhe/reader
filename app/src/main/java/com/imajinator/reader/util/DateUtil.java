package com.imajinator.reader.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class DateUtil {
    public static String adjustTimePattern(String date, String oldPattern, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldPattern, Locale.getDefault());
        try {
            Date oldPatternDate = dateFormat.parse(date);
            dateFormat.applyPattern(pattern);
            return dateFormat.format(oldPatternDate);
        } catch (ParseException | NullPointerException e) {
            return null;
        }
    }
}
