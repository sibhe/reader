package com.imajinator.reader.util;

import android.util.Log;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class LogHandler {
    public static void logDebug(String tag, String message) {
        Log.d(tag, message);
    }

}
