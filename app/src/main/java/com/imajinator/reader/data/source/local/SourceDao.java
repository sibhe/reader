package com.imajinator.reader.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.imajinator.reader.data.Source;

import org.reactivestreams.Publisher;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */
@Dao
public interface SourceDao {
    @Query("SELECT * FROM source")
    Publisher<List<Source>> getSelectedSources();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAll(List<Source> sources);

    @Query("DELETE FROM source")
    void flushTable();

    @Query("SELECT COUNT(*) FROM source")
    Publisher<Integer> count();
}
