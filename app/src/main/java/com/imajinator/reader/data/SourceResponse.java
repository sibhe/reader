package com.imajinator.reader.data;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class SourceResponse {
    private String status;
    private List<Source> sources;

    public String getStatus() {
        return status;
    }

    public List<Source> getSources() {
        return sources;
    }
}
