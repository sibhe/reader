package com.imajinator.reader.data.source.local;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.imajinator.reader.data.Source;

/**
 * Created by sibhe7 on 9/14/2017.
 */
@Database(entities = {Source.class}, version = 1)
public abstract class AppRoomDatabase  extends RoomDatabase{
    private static AppRoomDatabase sInstance;

    public abstract SourceDao getSourceDao();

    public static AppRoomDatabase getInstance(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(),
                    AppRoomDatabase.class, "reader-db").build();
        }

        return sInstance;
    }

    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    public static void destroyInstance() {
        sInstance = null;
    }
}
