package com.imajinator.reader.data.source;

import com.imajinator.reader.data.Article;
import com.imajinator.reader.data.Source;
import com.imajinator.reader.data.source.local.LocalDataSource;
import com.imajinator.reader.data.source.remote.RemoteDataSource;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class NewsRepository implements NewsDataSource {
    private LocalDataSource mLocalDataSource;
    private RemoteDataSource mRemoteDataSource;

    private static NewsRepository sInstance;

    private NewsRepository(LocalDataSource localDataSource, RemoteDataSource remoteDataSource) {
        mLocalDataSource = localDataSource;
        mRemoteDataSource = remoteDataSource;
    }

    public static NewsRepository getInstance(LocalDataSource localDataSource,
                                             RemoteDataSource remoteDataSource) {
        if (sInstance == null) {
            sInstance = new NewsRepository(localDataSource, remoteDataSource);
        }
        return sInstance;
    }


    public Observable<List<Article>> getArticles() {
        Observable<List<Source>> userSourcesObservable = getUserSelectedSources();

        return userSourcesObservable.flatMap(sources -> Observable.fromIterable(sources)
                .flatMap(source -> mRemoteDataSource.getArticles(source.getId())));
    }

    @Override
    public Observable<List<Article>> getArticles(String sources) {
        return mRemoteDataSource.getArticles(sources);
    }

    @Override
    public Observable<List<Source>> getSources() {
        return mRemoteDataSource.getSources();
    }

    @Override
    public Observable<Void> saveUserSelectedSources(List<Source> sources) {
        return mLocalDataSource.saveUserSelectedSources(sources);
    }

    @Override
    public Observable<List<Source>> getUserSelectedSources() {
        return mLocalDataSource.getUserSelectedSources();
    }

    @Override
    public Observable<Boolean> isSelectedSourceEmpty() {
        return mLocalDataSource.isSelectedSourceEmpty();
    }
}
