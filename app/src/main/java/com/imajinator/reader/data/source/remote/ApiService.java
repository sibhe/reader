package com.imajinator.reader.data.source.remote;

import com.imajinator.reader.data.ArticleResponse;
import com.imajinator.reader.data.SourceResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public interface ApiService {
    String apiVersion = "v1";

    @GET(apiVersion + "/articles")
    Observable<ArticleResponse> getArticles(@Query("source") String source);

    @GET(apiVersion + "/sources?language=en")
    Observable<SourceResponse> getSources();
}
