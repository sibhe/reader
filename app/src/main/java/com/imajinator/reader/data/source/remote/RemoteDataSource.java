package com.imajinator.reader.data.source.remote;

import android.content.Context;

import com.imajinator.reader.data.Article;
import com.imajinator.reader.data.ArticleResponse;
import com.imajinator.reader.data.Source;
import com.imajinator.reader.data.SourceResponse;
import com.imajinator.reader.data.source.NewsDataSource;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class RemoteDataSource implements NewsDataSource {
    private static RemoteDataSource sInstance = null;
    private ApiService mApiService;

    private RemoteDataSource(Context context) {
        mApiService = ApiHelper.getInstance(context).provideRetrofit()
                .create(ApiService.class);
    }

    public static RemoteDataSource getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RemoteDataSource(context);
        }

        return sInstance;
    }

    @Override
    public Observable<List<Article>> getArticles(String sources) {
        return mApiService.getArticles(sources)
                .map(ArticleResponse::getArticles);
    }

    @Override
    public Observable<List<Article>> getArticles() {
        return mApiService.getArticles(null)
                .map(ArticleResponse::getArticles);
    }

    @Override
    public Observable<List<Source>> getSources() {
        return mApiService.getSources().map(SourceResponse::getSources);
    }

    @Override
    public Observable<Void> saveUserSelectedSources(List<Source> selectedSources) {
        throw new RuntimeException("Function not implemented");
    }

    @Override
    public Observable<List<Source>> getUserSelectedSources() {
        throw new RuntimeException("Function not implemented");
    }

    @Override
    public Observable<Boolean> isSelectedSourceEmpty() {
        throw new RuntimeException("Function not implemented");
    }
}
