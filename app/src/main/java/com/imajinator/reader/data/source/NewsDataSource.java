package com.imajinator.reader.data.source;

import com.imajinator.reader.data.Article;
import com.imajinator.reader.data.Source;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public interface NewsDataSource {

    Observable<List<Article>> getArticles(String sources);

    Observable<List<Article>> getArticles();

    Observable<List<Source>> getSources();

    Observable<Void> saveUserSelectedSources(List<Source> selectedSources);

    Observable<List<Source>> getUserSelectedSources();

    Observable<Boolean> isSelectedSourceEmpty();
}
