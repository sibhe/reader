package com.imajinator.reader.data;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class ArticleResponse {
    private String status;
    private String source;
    private String sortBy;
    private List<Article> articles;

    public String getStatus() {
        return status;
    }

    public String getSource() {
        return source;
    }

    public String getSortBy() {
        return sortBy;
    }


    public List<Article> getArticles() {
        return articles;
    }
}
