package com.imajinator.reader;

import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class BaseBindingViewHolder extends RecyclerView.ViewHolder {
    ViewDataBinding binding;

    public BaseBindingViewHolder(ViewDataBinding layoutBinding, @Nullable View.OnClickListener listener) {
        super(layoutBinding.getRoot());
        binding = layoutBinding;
        binding.getRoot().setOnClickListener(listener);
        binding.getRoot().setTag(this);
    }


    public ViewDataBinding getBinding() {
        return binding;
    }

}
