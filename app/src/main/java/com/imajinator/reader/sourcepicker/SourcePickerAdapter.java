package com.imajinator.reader.sourcepicker;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.imajinator.reader.BaseBindingAdapter;
import com.imajinator.reader.R;
import com.imajinator.reader.data.Source;
import com.imajinator.reader.databinding.ItemSourceBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class SourcePickerAdapter extends BaseBindingAdapter {
    private List<Source> mSources;
    private List<Source> mSelectedSources;
    private View.OnClickListener mListener;

    public SourcePickerAdapter(List<Source> sources, View.OnClickListener listener) {
        mSources = sources;
        mListener = listener;
        mSelectedSources = new ArrayList<>();
    }

    @Override
    protected void updateBinding(ViewDataBinding binding, int position) {
        ItemSourceBinding itemBinding = (ItemSourceBinding) binding;
        Source source = mSources.get(position);
        itemBinding.tvSource.setText(source.getName());
        itemBinding.getRoot().setSelected(mSelectedSources.contains(source));
    }

    List<Source> getSelectedSources() {
        return mSelectedSources;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.item_source;
    }

    @Override
    protected View.OnClickListener getOnClickListener() {
        return v -> {
            v.setSelected(!v.isSelected());

            int selectedPos = ((RecyclerView.ViewHolder) v.getTag()).getAdapterPosition();
            Source selectedSource = mSources.get(selectedPos);

            if (v.isSelected()) {
                if (!mSelectedSources.contains(selectedSource)) {
                    mSelectedSources.add(selectedSource);
                }
            } else {
                if (mSelectedSources.contains(selectedSource)) {
                    mSelectedSources.remove(selectedSource);
                }
            }

            if (mListener != null) {
                mListener.onClick(v);
            }
        };
    }

    @Override
    public int getItemCount() {
        return mSources.size();
    }
}
