package com.imajinator.reader.sourcepicker;

import com.imajinator.reader.BasePresenter;
import com.imajinator.reader.BaseView;
import com.imajinator.reader.data.Source;

import java.util.List;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class SourcePickerContract {

    interface View extends BaseView<Presenter> {
        void showSources(List<Source> sources);

        void showErrorView();

        void showProgressBar();

        void hideProgressBar();

        void goToNewsReader();
    }

    interface Presenter extends BasePresenter<View> {
        void getSources();

        void saveSelectedSources(List<Source> selectedSources);
    }
}
