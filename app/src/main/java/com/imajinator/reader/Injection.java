package com.imajinator.reader;

import android.content.Context;
import android.support.annotation.NonNull;

import com.imajinator.reader.data.source.NewsRepository;
import com.imajinator.reader.data.source.local.LocalDataSource;
import com.imajinator.reader.data.source.remote.RemoteDataSource;
import com.imajinator.reader.util.schedulers.BaseSchedulerProvider;
import com.imajinator.reader.util.schedulers.SchedulerProvider;

/**
 * Created by sibhe7 on 9/14/2017.
 */

public class Injection {

    public static NewsRepository provideRepository(@NonNull Context context) {
        return NewsRepository.getInstance(LocalDataSource.getInstance(context.getApplicationContext()),
                RemoteDataSource.getInstance(context.getApplicationContext()));
    }

    public static BaseSchedulerProvider provideScheduler(){
        return SchedulerProvider.getInstance();
    }
}
